require 'simplecov'
SimpleCov.start

require 'minitest/spec'
require 'minitest/autorun'

require 'rubycron/entry'

describe RubyCron::Entry::Base do
  before do
    @entry = RubyCron::Entry::Base.new
  end

  describe 'closest' do
    it 'should work in a simple case' do
      assert_equal [3, 0], @entry.closest(2, [1, 3], 4)
    end

    it 'should rollover' do
      assert_equal [1, 1], @entry.closest(4, [1, 3], 4)
    end

    it 'should handle modulo' do
      assert_equal [1, 1], @entry.closest(5, [1, 3], 4)  # 5 / 4 = 1
    end
  end

  describe 'next' do
    it 'should work for the next minute' do
      t = Time.at(1422631878).utc # 2015-01-30 15:31:18
      entry = RubyCron::Entry::Vixie.new('32 * * * *')
      assert_equal Time.at(1422631920).utc, entry.next(t).first # => 2015-01-30 15:32:00
    end

    it 'should work for the next hour' do
      t = Time.at(1422631878).utc # 2015-01-30 15:31:18
      entry = RubyCron::Entry::Vixie.new('32 16 * * *')
      assert_equal Time.at(1422635520).utc, entry.next(t).first # 2015-01-30 16:32:00
    end

    it 'should roll over to the next hour when minute is before this minute' do
      t = Time.at(1422631878).utc # 2015-01-30 15:31:18
      entry = RubyCron::Entry::Vixie.new('30 * * * *')
      assert_equal Time.at(1422635400).utc, entry.next(t).first # 2015-01-30 16:30:00
    end

    it 'should roll over to the next month when day is before today' do
      t = Time.at(1422631878).utc # 2015-01-30 15:31:18
      entry = RubyCron::Entry::Vixie.new('0 5 5 * *')
      assert_equal Time.at(1423112400).utc, entry.next(t).first # 2015-02-05 05:00:00 UTC
    end

    it 'should not work with impossible days, e.g. February 30' do
      t = Time.at(1425092400).utc # 2015-02-28 03:00:00
      entry = RubyCron::Entry::Vixie.new('1 2 30 * *')
      assert_equal Time.at(1427680860).utc, entry.next(t).first # 2015-03-30 02:01:00 UTC
    end

    it 'should not work with possible days, e.g. March 31' do
      t = Time.at(1427680800).utc # 2015-03-30 02:00:00 UTC
      entry = RubyCron::Entry::Vixie.new('1 2 31 * *')
      assert_equal Time.at(1427767260).utc, entry.next(t).first # 2015-03-31 02:01:00 UTC
    end

    it 'should roll over to the next month in months with 31 days' do
      t = Time.at(1422718278).utc # 2015-01-31 15:31:18
      entry = RubyCron::Entry::Vixie.new('0 2 * * *')
      assert_equal Time.at(1422756000).utc, entry.next(t).first # 2015-02-01 02:00:00
    end

    it 'should roll over to the next month in months with 28 days' do
      t = Time.at(1425092400).utc # 2015-02-28 03:00:00
      entry = RubyCron::Entry::Vixie.new('0 2 * * *')
      assert_equal Time.at(1425175200).utc, entry.next(t).first # 2015-03-01 02:00:00
    end

    it 'should roll over to the next year if necessary' do
      t = Time.at(1425092400).utc # 2015-02-28 03:00:00
      entry = RubyCron::Entry::Vixie.new('1 2 1 1 *')
      assert_equal Time.at(1451613660).utc, entry.next(t).first # 2016-01-01 02:01:00
    end
  end
end
