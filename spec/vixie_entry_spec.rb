require 'simplecov'
SimpleCov.start

require 'minitest/spec'
require 'minitest/autorun'

require 'rubycron/entry'

describe RubyCron::Entry::Vixie do
  describe 'parse_field' do
    before do
      @entry = RubyCron::Entry::Vixie.new(nil)
    end

    it 'handles basic numbers' do
      assert_equal [1], @entry.parse_field('1', 0, 7)
    end

    it 'handles comma-separated numbers' do
      assert_equal [1, 3, 5], @entry.parse_field('1,3,5', 0, 7)
    end

    it 'handles *' do
      assert_equal [0, 1, 2, 3, 4], @entry.parse_field('*', 0, 4)
    end

    it 'handles * with modulo' do
      assert_equal [0, 3, 6, 9], @entry.parse_field('*/3', 0, 9)
    end

    it 'handles comma-separated * with modulo' do
      assert_equal [0, 3, 4, 6, 8, 9], @entry.parse_field('*/3,*/4', 0, 9)
    end

    it 'handles ranges' do
      assert_equal [4, 5, 6], @entry.parse_field('4-6', 0, 9)
    end

    it 'handles comma-separated ranges' do
      assert_equal [2, 3, 5, 6, 7], @entry.parse_field('2-3,5-7', 0, 9)
    end

    it 'handles comma-separated range + number' do
      assert_equal [2, 3, 7], @entry.parse_field('2-3,7', 0, 9)
    end

    it 'throws errors out of range' do
      assert_raises RubyCron::BadEntryException do
        @entry.parse_field('10', 0, 9)
      end
    end

    it 'throws errors for bad range' do
      assert_raises RubyCron::BadEntryException do
        @entry.parse_field('2-10', 0, 9)
      end
    end

    it 'throws errors for comma out of range' do
      assert_raises RubyCron::BadEntryException do
        @entry.parse_field('1,2,100', 0, 9)
      end
    end

    it 'throws errors for 0 out of range' do
      assert_raises RubyCron::BadEntryException do
        @entry.parse_field('0', 1, 9)
      end
    end
  end

  describe 'parse' do
    before do
      @entry = RubyCron::Entry::Vixie.new(nil)
    end

    it 'should work for 5 numbers' do
      @entry.parse('1 2 3 4 5')
      assert_equal [1], @entry.minutes
      assert_equal [2], @entry.hours
      assert_equal [2], @entry.days
      assert_equal [3], @entry.months
      assert_equal [5], @entry.weekdays
    end

    it 'should error for 4 numbers' do
      assert_raises RubyCron::BadEntryException do
        @entry.parse('1 2 3 4')
      end
    end

    it 'should error for 6 numbers' do
      assert_raises RubyCron::BadEntryException do
        @entry.parse('1 2 3 4 5 6')
      end
    end

    it 'should work for 5 ranges' do
      @entry.parse('1-2 2-3 3-4 4-5 5-6')
      assert_equal [1,2], @entry.minutes
      assert_equal [2,3], @entry.hours
      assert_equal [2,3], @entry.days
      assert_equal [3,4], @entry.months
      assert_equal [5,6], @entry.weekdays
    end

    it 'should fail for minutes out of range' do
      assert_raises RubyCron::BadEntryException do
        @entry.parse('60 0 1 1 0')
      end
    end

    it 'should fail for hours out of range' do
      assert_raises RubyCron::BadEntryException do
        @entry.parse('0 24 1 1 0')
      end
    end

    it 'should fail for days out of range' do
      assert_raises RubyCron::BadEntryException do
        @entry.parse('0 0 0 1 0')
      end
      assert_raises RubyCron::BadEntryException do
        @entry.parse('0 0 32 1 0')
      end
    end

    it 'should fail for months out of range' do
      assert_raises RubyCron::BadEntryException do
        @entry.parse('0 0 1 0 0')
      end
      assert_raises RubyCron::BadEntryException do
        @entry.parse('0 0 1 13 0')
      end
    end

    it 'should fail for weekdays out of range' do
      assert_raises RubyCron::BadEntryException do
        @entry.parse('0 0 1 1 7')
      end
    end
  end
end
