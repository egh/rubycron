module RubyCron
  class BadEntryException < Exception
  end

  module Entry
    class Base
      attr_accessor :minutes, :hours, :days, :months, :weekdays

      def self.check_range(i, min, max)
        fail BadEntryException, "#{i} not in range #{min}-#{max}" if i < min || i > max
        i
      end

      COMMON_MONTH_MODULOS = [nil, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]
      def month_modulo(month, year)
        return 29 if month == 2 && Date.gregorian_leap?(year)
        COMMON_MONTH_MODULOS[month]
      end

      def next(t)
        Enumerator.new do |enum|
          loop do
            t = t.getutc

            minute, rollover_hour = closest(t.min + 1, @minutes, 60)
            hour, rollover_day = closest(t.hour + rollover_hour, @hours, 24)
            day, rollover_month = closest(t.day - 1 + rollover_day, @days,
                                          month_modulo(t.month, t.year))
            # TODO: weekday handling
            month, rollover_year = closest(t.month - 1 + rollover_month, @months, 12)
            t = Time.utc(t.year + rollover_year, month + 1, day + 1, hour, minute, 0, 0)
            enum.yield t
          end
        end
      end

          
      # Find the nearest value from allowed, an integer array, to n1 %
      # mod. "Closest" means either the nearest following n1 % mod or
      # the earliest. Returns (nearest, rollover) where rollover is 1
      # if n1 / mod is 1 OR the nearest value in allowed was < n1 %
      # mod.
      # Basically, closest(2, [1, 3], 4) => (3, 0)
      #            closest(3, [1, 3], 4) => (3, 0)
      #            closest(4, [1, 3], 4) => (1, 1)
      #            closest(5, [1, 3], 4) => (1, 1) <- because 5 / 4 = 1
      def closest(n1, allowed, mod)
        n = n1 % mod
        rollover = n1 / mod
        before, after = allowed.partition { |i| i < n }.map(&:sort)
        if after.first
          [after.first, rollover]
        else
          [before.first, 1]
        end
      end
    end

    # Implements "classic" Vixie cron entries
    class Vixie < Base
      def initialize(str)
        parse(str) unless str.nil?
      end

      def parse(str)
        fields = str.split(/\s+/)
        fail BadEntryException, "Bad Vixie entry: '#{str}'" unless fields.size == 5
        @minutes = parse_field(fields[0], 0, 59)
        @hours = parse_field(fields[1], 0, 23)
        # use 0-indexed for storage
        @days = parse_field(fields[2], 1, 31).map {|i| i - 1 }
        @months = parse_field(fields[3], 1, 12).map {|i| i - 1 }
        @weekdays = parse_field(fields[4], 0, 6)
      end

      def parse_field(str, min, max)
        # handle x,y entries first
        if str.match(/,/)
          return str.split(/,/).map do |e|
            parse_field(e, min, max)
          end.flatten.sort.uniq
        else
          case str
          when /^\*\/(\d+)$/
            step = $1.to_i
            (min..max).find_all { |n| (n % step) == 0 }
          when /^(\d+)-(\d+)$/
            range_min = Base.check_range($1.to_i, min, max)
            range_max = Base.check_range($2.to_i, min, max)
            (range_min..range_max).to_a
          when /^\*$/
            (min..max).to_a
          when /^\d+$/
            [Base.check_range(str.to_i, min, max)]
          end
        end
      end
    end
  end
end
