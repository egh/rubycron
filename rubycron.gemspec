# -*- encoding: utf-8 -*-
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
$:.push File.expand_path('../lib', __FILE__)

Gem::Specification.new do |s|
  s.name          = 'rubycron'
  s.version       = '0.0.1'
  s.authors       = ['Erik Hetzner']
  s.email         = ['egh@e6h.org']
  s.license       = 'LGPL'
  s.homepage      = ''
  s.summary       = 'Scheduled jobs in Ruby using cron syntax.'
  s.description   = 'Clean ruby syntax for writing and deploying cron jobs.'
  s.files         = `git ls-files`.split($/)
  s.test_files    = `git ls-files -- spec/*`.split('\n')
  s.require_paths = ['lib']
  s.add_development_dependency 'bundler', '~> 1.3'
  s.add_development_dependency 'rake'
  s.add_development_dependency 'simplecov'
end
